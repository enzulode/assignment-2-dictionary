import unittest
import subprocess

class DictionaryUnitTest(unittest.TestCase):
    def execute(self, input):
        pipe = subprocess.PIPE
        proc = subprocess.Popen(
            ["./test/main"],
            text=True,
            shell=True,
            stdin=pipe,
            stdout=pipe,
            stderr=pipe
        )
        stdout, stderr = proc.communicate(input)
        return (stdout.strip(), stderr.strip())
    
    # Tesing that values for valid keys are successfully found
    def test_valid_key(self):
        inputs = ["one", "two", "three", "four", "five", "six"]
        outputs = ["first", "second", "third", "fourth", "fifth", "sixth"]

        for i in range(0, len(inputs)):
            self.assertEqual(self.execute(inputs[i]), (outputs[i], ""))

    # Testing that values for invalid keys return an error in stderr
    def test_not_existing_key(self):
        inputs = ["a", "b", "c"]
        output = "Error: no such element for provided key"

        for i in range(0, len(inputs)):
            self.assertEqual(self.execute(inputs[i]), ("", output))

    # Testing that too long key won't be accepted
    def test_invalid_key(self):
        inputs = ["asddddddddddddddddddddddddddddddddddddddadasdasddddddddddddddddddddddddddddddddddddddadasddddddddddddddddddddddddddddddddddddddadasdasddddddddddddddddddddddddddddddddddddddadasddddddddddddddddddddddddddddddddddddddadasdasddddddddddddddddddddddddddddddddddddddadasddddddddddddddddddddddddddddddddddddddadasdasddddddddddddddddddddddddddddddddddddddad"]
        output = "Error: the key string is too long"

        for i in range(0, len(inputs)):
            self.assertEqual(self.execute(inputs[i]), ("", output))

    

#   Running unit tests
if __name__ == "__main__":
    unittest.main()