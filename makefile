# Directories
SRC_DIR=src
BUILD_DIR=build
TEST_DIR=test

# Properties
DEBUG=true
FORMAT=elf64

# Service properties
DEBUG_FLAGS=
ifeq ($(strip $(DEBUG)), true)
	DEBUG_FLAGS=-g
endif

ASM_FILES=$(wildcard $(SRC_DIR)/*.asm)
OBJ_FILES=$(ASM_FILES:$(SRC_DIR)/%.asm=$(BUILD_DIR)/%.o)

# Targets
.PHONY:	clean run test
clean:
	rm -rf $(BUILD_DIR)/*.o
	rm -rf $(BUILD_DIR)/main

test: clean build
	mv $(BUILD_DIR)/main $(TEST_DIR)/main
	python3 $(TEST_DIR)/dictionary_unit_test.py
	rm -rf $(TEST_DIR)/main
	rm -rf $(BUILD_DIR)/*.o
	rm -rf $(BUILD_DIR)/main

$(BUILD_DIR)/%.o:	$(SRC_DIR)/%.asm
	nasm $(DEBUG_FLAGS) -f $(FORMAT) -o $@ $<

$(BUILD_DIR)/main.o:	$(SRC_DIR)/main.asm	$(wildcard $(SRC_DIR)/*.inc)

build:	$(OBJ_FILES)
	ld -o $(BUILD_DIR)/main $^

run: build
	chmod a+x $(BUILD_DIR)/main
	./$(BUILD_DIR)/main