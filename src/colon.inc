%define last_ptr 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
            dq last_ptr
            db %1, 0

            %define last_ptr %2
        %else
            %error "Second argument expected to be identifier: something else found"
        %endif
    %else
        %error "First argument expected to be string: something else found"
    %endif
%endmacro