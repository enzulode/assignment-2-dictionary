%include "src/colon.inc"
%include "src/lib.inc"
%include "src/words.inc"
%include "src/dict.inc"

%define ALLOCATED_SPACE 256

section .data
error_long_key: db "Error: the key string is too long", 10, 0
error_element_not_found: db "Error: no such element for provided key", 10, 0

section .text
global _start

_start:
    sub rsp, ALLOCATED_SPACE            ;   allocate stack space

    mov rdi, rsp                        ;
    mov rsi, ALLOCATED_SPACE - 1        ;   read user input into allocated space on stack
    call read_word                      ;

    test rax, rax                       ;
    mov rdi, error_long_key             ;   check if user input is too long
    jz .error                           ;   then just finish with error

    mov rdi, rax                        ;
    mov rsi, first_el                   ;   searching word in dictionary
    call find_word                      ;

    test rax, rax                       ;
    mov rdi, error_element_not_found    ;   check if element was not found
    jz .error                           ;   then just finish with error

    mov rdi, rax                        ;   if no errors were found - rax contains element pointer

    push rdi                            ;
    call string_length                  ;   calculating element key length for skipping it
    pop rdi                             ;

    inc rax
    add rdi, rax                        ;   skipping element key
    call print_string                   ;   and printing value for requested key 

    xor rdi, rdi                        ;   set exit code to 0 (success)
    jmp .end

    .error:
        call print_error                ;   print error message
        mov rdi, 1                      ;   and set exit code to 1 (failed)

    .end:
        add rsp, ALLOCATED_SPACE        ;   restore stack space
        call exit                       ;   finish
