%include "src/lib.inc"

%define WORDSIZE 8

section .text
global find_word

; Consumes null-terminated key string pointer (rdi) and dictionary start pointer (rsi)
; succeed - returns found dictionary entry pointer, failed - returns 0
find_word:
    ; RDI       ------      key string pointer
    ; RSI       ------      dictionary start pointer

    push r9                         ;
    push rsi                        ;   save registers
    mov r9, rsi                     ;

    .loop:
        add r9, WORDSIZE            ;
        mov rsi, r9                 ;   check if first element is an expected target
        call string_equals          ;

        test rax, rax               ;   if returned 1 - it means that the target was found 
        jnz .found                  ;

        sub r9, WORDSIZE            ;   then just go to the next element
        mov r9, [r9]                ;

        test r9, r9                 ;   check if the dictionary ended
        jz .error                   ;   it means, that the target wasn't found: error

        jmp .loop                   ;   next iteration

    .error:
        xor rax, rax                ;   just return 0 in rax
        jmp .end

    .found:
        mov rax, rsi                ;   return found value pointer in rax

    .end:
        pop rsi                     ;
        pop r9                      ;   restore registers and return
        ret                         ;